package com.accenture.eshop.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import com.accenture.eshop.model.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long>{
}
