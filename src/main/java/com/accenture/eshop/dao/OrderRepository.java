package com.accenture.eshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.accenture.eshop.model.Orders;

public interface OrderRepository extends JpaRepository<Orders, Long>{

}
