package com.accenture.eshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.eshop.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{

}
