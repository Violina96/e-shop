package com.accenture.eshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.eshop.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

	User findByUsername(String username);
//	User loginUser(User user);
}
