package com.accenture.eshop.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.accenture.eshop.model.AppStatus;


public class CommentDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private Long authorId;
	private Long productId;
	private String text;
	private String url;
	private AppStatus approvementStatus;
	private LocalDateTime created = LocalDateTime.now();
	private LocalDateTime modified = LocalDateTime.now();

	public CommentDto() {
	}

	public CommentDto(long authorId, long productId, String text, AppStatus approvementStatus) {
		this.authorId = authorId;
		this.productId = productId;
		this.text = text;
		this.approvementStatus = approvementStatus;
	}

	public CommentDto(long authorId, long productId, String text, String url, AppStatus approvementStatus) {
		this.authorId = authorId;
		this.productId = productId;
		this.text = text;
		this.url = url;
		this.approvementStatus = approvementStatus;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public AppStatus getApprovementStatus() {
		return approvementStatus;
	}

	public void setApprovementStatus(AppStatus approvementStatus) {
		this.approvementStatus = approvementStatus;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentDto other = (CommentDto) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("| Author: %12.12s | %s | %12.12s | %30.30s | %30.30s |",
				authorId, text, approvementStatus, created, modified);
	}

}
