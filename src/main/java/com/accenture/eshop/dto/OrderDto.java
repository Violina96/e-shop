package com.accenture.eshop.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import com.accenture.eshop.model.OrderStatus;
import com.accenture.eshop.model.Position;

public class OrderDto implements Comparable<OrderDto>, Serializable {

	private static final long serialVersionUID = 1L;
	@Column(name = "id")
	private Long id;
	@ManyToOne
	private Long userId;
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private OrderStatus status;
	@Column(name = "closing_date")
	private LocalDateTime closingDate;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Column(name = "positions")
	private Set<Position> positions = new LinkedHashSet<>();
	@Column(name = "description")
	@Size(min = 10, max = 500, message = "Description should be between 10 and 500 characters long.")
	private String description;
	@Column(name = "created")
	private LocalDateTime created = LocalDateTime.now();
	@Column(name = "last_modified")
	private LocalDateTime modified = LocalDateTime.now();
	
	public OrderDto() {}

	public OrderDto(long userId, Set<Position> positions) {
		this.userId = userId;
		this.positions = positions;
	}
	
	public OrderDto(long userId, String description, Set<Position> positions) {
		super();
		this.userId = userId;
		this.description = description;
		this.positions = positions;
	}

//	public void addPosition(Product product, int quantity){
//		positions.add(new Position(product, quantity));
//	}
	
	public void addPosition(Position pos) {
		positions.add(pos);
	}
	
	public void deletePosition(Position pos) {
		positions.remove(pos);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public LocalDateTime getClosingDate() {
		return closingDate;
	}

	public void setClosingDate(LocalDateTime closingDate) {
		this.closingDate = closingDate;
	}

	public Set<Position> getPositions() {
		return positions;
	}

	public void setPositions(Set<Position> positions) {
		this.positions = positions;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String descriptions) {
		this.description = descriptions;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderDto other = (OrderDto) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format(
				"| %12.12s |  %12.12s  | %12.12s | Positions: %s | %20.20s | %30.30s | %30.30s |",
				id, userId, closingDate, getPositions(), description, created, modified);
	}
	
	@Override
	public int compareTo(OrderDto other) {
		return Long.compare(this.getId(), other.getId());
	}
	
	public double getTotal() {
		double sum = 0;
		for(Position p: positions){
			sum += p.getPositionPrice();
		}
		return sum;
	}
}
