package com.accenture.eshop.exceptions;

public class EntityExistsException extends Exception {

	private static final long serialVersionUID = 1L;

	public EntityExistsException() {
		super();
	}

	public EntityExistsException(String message, Throwable cause) {
		super(message, cause);
	}

	public EntityExistsException(String message) {
		super(message);
	}

	public EntityExistsException(Throwable cause) {
		super(cause);
	}

}
