package com.accenture.eshop.exceptions;

public class NonExistingEntityException extends Exception {

	private static final long serialVersionUID = 1L;

	public NonExistingEntityException() {
		super();
	}

	public NonExistingEntityException(String message, Throwable cause) {
		super(message, cause);
	}

	public NonExistingEntityException(String message) {
		super(message);
	}

	public NonExistingEntityException(Throwable cause) {
		super(cause);
	}

}
