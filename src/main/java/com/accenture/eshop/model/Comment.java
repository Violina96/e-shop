package com.accenture.eshop.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class Comment implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@ManyToOne
	private User author;
	@ManyToOne
	private Product product;
	@Column(name = "text", nullable = false)
	@Size(min = 5, max = 1500, message = "Text should be between 5 and 1500 characters long.")
	private String text;
	@Pattern(regexp = "^$|(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$", message = "Please provide a valid url!")
	private String url;
	@Enumerated(EnumType.STRING)
	@Column(name = "approvementStatus")
	private AppStatus approvementStatus;
	@Column(name = "created")
	private LocalDateTime created = LocalDateTime.now();
	@Column(name = "last_modified")
	private LocalDateTime modified = LocalDateTime.now();

	public Comment() {
	}

	public Comment(User author, Product product, String text, AppStatus approvementStatus) {
		this.author = author;
		this.product = product;
		this.text = text;
		this.approvementStatus = approvementStatus;
	}

	public Comment(User author, Product product, String text, String url, AppStatus approvementStatus) {
		this.author = author;
		this.product = product;
		this.text = text;
		this.url = url;
		this.approvementStatus = approvementStatus;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public AppStatus getApprovementStatus() {
		return approvementStatus;
	}

	public void setApprovementStatus(AppStatus approvementStatus) {
		this.approvementStatus = approvementStatus;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

	
//	@Override
//	public String toString() {
//		return String.format("| Author: %s | Product: %s | %s | %12.12s | %30.30s | %30.30s |", 
//				author, product, text, approvementStatus, created, modified);
//	}
	
	@Override
	public String toString() {
		return String.format("| %12.12s | %s | %12.12s | %30.30s | %30.30s |", id,
				 text, approvementStatus, created, modified);
	}

}
