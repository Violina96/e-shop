package com.accenture.eshop.model;

public enum OrderStatus {

	ACTIVE, COMPLETED, CANCELED;
	
}
