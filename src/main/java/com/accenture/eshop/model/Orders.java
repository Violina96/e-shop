package com.accenture.eshop.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;

@Entity
public class Orders implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@ManyToOne
	private User user;
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	@Valid
	private OrderStatus status;
	@Column(name = "closing_date")
	private LocalDateTime closingDate;
	@Column(name = "description", nullable = false)
	@Size(min = 10, max = 500, message = "Description should be between 10 and 500 characters long.")
	private String description;
	@Column(name = "created")
	private LocalDateTime created = LocalDateTime.now();
	@Column(name = "last_modified")
	private LocalDateTime modified = LocalDateTime.now();
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Column(name = "positions")
	private Set<Position> positions = new LinkedHashSet<>();

	public Orders() {
	}

	public Orders(User user, Set<Position> positions) {
		this.user = user;
		this.positions = positions;
	}

	public Orders(User user, String description, Set<Position> positions) {
		super();
		this.user = user;
		this.description = description;
		this.positions = positions;
	}

	public void addPosition(Position pos) {
		positions.add(pos);
	}

	public void deletePosition(Position pos) {
		positions.remove(pos);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public LocalDateTime getClosingDate() {
		return closingDate;
	}

	public void setClosingDate(LocalDateTime closingDate) {
		this.closingDate = closingDate;
	}

	public Set<Position> getPositions() {
		return positions;
	}

	public void setPositions(Set<Position> positions) {
		this.positions = positions;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String descriptions) {
		this.description = descriptions;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orders other = (Orders) obj;
		if (id != other.id)
			return false;
		return true;
	}

//	@Override
//	public String toString() {
//		return String.format(
//				"| %12.12s | User: %12.12s  | %12.12s | Positions: %s | %20.20s | %30.30s | %30.30s |",
//				id, user.getName(), closingDate, getPositions(), description, created, modified);
//	}

	public double getTotal() {
		double sum = 0;
		for (Position p : positions) {
			sum += p.getPositionPrice();
		}
		return sum;
	}

}
