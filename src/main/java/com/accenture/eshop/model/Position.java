package com.accenture.eshop.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;

@Entity
public class Position implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@OneToOne
	private Orders order;
	@OneToOne
	private Product product;
	@Column(name = "quantity", nullable = false)
	@DecimalMin(value = "0.1", inclusive = true, message = "Please enter quantity > 0")
	private Double quantity;
	@DecimalMin(value = "0.1", inclusive = true, message = "Please enter price > 0")
	private Double price;
	@Column(name = "created")
	private LocalDateTime created = LocalDateTime.now();
	@Column(name = "last_modified")
	private LocalDateTime modified = LocalDateTime.now();

	public Position() {}
	
	public Position(Product product, double quantity, Double price) {
		this.product = product;
		this.quantity = quantity;
		this.price = price;
	}
	
	public Position(Product product, double quantity) {
		this.product = product;
		this.quantity = quantity;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Orders getOrder() {
		return order;
	}

	public void setOrder(Orders order) {
		this.order = order;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}
	
	public Double getPositionPrice() {
		return quantity * (price != null ? price : product.getPrice());
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return String.format("id=%s | %3.3s | %5.5s | %30.30s | %30.30s |", id,
				 quantity, price, created, modified);
	}
	
	

}
