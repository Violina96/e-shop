package com.accenture.eshop.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

@Entity
public class Product implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@Column(name = "name", nullable = false)
	@Size(min = 2, max = 50, message = "Name should be between 2 and 50 characters long.")
	private String name;
	@Column(name = "description", nullable = false)
	@Size(min = 10, max = 500, message = "Desription should be between 10 and 500 characters long.")
	private String description;
	@Column(name = "price", nullable = false)
	@DecimalMin(value = "0.1", inclusive = true, message = "Please enter price > 0")
	private Double price;
	@Column(name = "is_promoted")
	private Boolean isPromoted;
	@Column(name = "promotion_percentage")
	@DecimalMin(value = "0.1", inclusive = true, message = "Please enter percentage > 0")
	@DecimalMax(value = "99.99", inclusive = true, message = "Please enter percentage < 100")
	private Double promotionPercentage;
	@Enumerated(EnumType.STRING)
	@Column(name = "unit", nullable = false)
	@Valid
	private Unit unit;
	@Column(name = "tags")
	private String tags;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Comment> comments = new LinkedHashSet<>();
	@Column(name = "created")
	private LocalDateTime created = LocalDateTime.now();
	@Column(name = "last_modified")
	private LocalDateTime modified = LocalDateTime.now();
	
	
	public Product() {}
	
	public Product(String name, String description, double price, Unit unit, String tags) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.unit = unit;
		this.tags = tags;
	}


	public Product(String name, String description, double price, boolean isPromoted, double promotionPercentage,
			Unit unit, String tags, Set<Comment> comments) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.isPromoted = isPromoted;
		this.promotionPercentage = promotionPercentage;
		this.unit = unit;
		this.tags = tags;
		this.comments = comments;
	}
	
	public void addComment(Comment cm) {
		comments.add(cm);
	}
	
	public void removeComment(Comment cm) {
		comments.remove(cm);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getIsPromoted() {
		return isPromoted;
	}

	public void setIsPromoted(Boolean isPromoted) {
		this.isPromoted = isPromoted;
	}
	
	public Double getPromotionPercentage() {
		return promotionPercentage;
	}

	public void setPromotionPercentage(Double promotionPercentage) {
		this.promotionPercentage = promotionPercentage;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format(
				"| %12.12s | %20.20s | %30.30s | %5.5s | %5.5s | %5.5s | %3.3s | %20.20s | Comments: { %s } | %30.30s | %30.30s |",
				id, name, description, price, isPromoted, promotionPercentage, unit, tags, comments, created, modified);
	}
		
}
