package com.accenture.eshop.model;

public enum Unit {
	PCS, KG, M, L
}
