package com.accenture.eshop.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Entity
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@Column(name = "name", nullable = false)
	@Size(min=2, max=50, message = "Name should be between 2 and 50 characters long!")
	private String name;
	@Column(name = "email", unique = true, nullable = false)
	@Pattern(regexp = "^[a-zA-Z0-9_!#$%&�*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", message = "Please provide a valid Email, e.g. ivan@abv.bg!")
	private String email;
	@Column(name = "username", unique = true, nullable = false)
	@Pattern(regexp = "^(?=.{2,15}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$", message = "Username should be between 2 and 15 characters long!")
	private String username;
	@Column(name = "password", nullable = false)	
	private String password;
	@Transient
	@Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{8,15}$", message = "Password should be between 8 and 15 characters long, containing one upper case letter, some character[!@#$%^&+=] and at least one digit!")
	private String confirmPasword;
	@Enumerated(EnumType.STRING)
	@Column(name = "gender", nullable = false)
	private Gender gender;
	@Enumerated(EnumType.STRING)
	@Column(name = "role")
	private Role role;
	@Pattern(regexp = "^$|(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$", message = "Please provide a valid url!")
	private String picture;
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private Status status;
	@Column(name = "metadata")
	@Size(max=512, message = "Metadata can be up to 512 characters long.")
	private String metadata;
	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false)
	private UserType type;
	@Column(name = "ssn_or_vat", nullable = false)
	@Size(min=9, max=16, message = "Ssn or Vat should be between 9 and 16 characters long.")
	private String ssnVat;
	@Column(name = "address", nullable = false)
	@Size(min=10, max=250, message = "Address should be between 10 and 250 characters long.")
	private String address;
	@Column(name = "created")
	private LocalDateTime created = LocalDateTime.now();
	@Column(name = "last_modified")
	private LocalDateTime modified = LocalDateTime.now();
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Column(name = "products_added")
	private Set<Product> productsAdded = new LinkedHashSet<>();
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Column(name = "orders")
	private Set<Orders> orders = new LinkedHashSet<>();
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Column(name = "comments")
	private Set<Comment> comments = new LinkedHashSet<>();;

	public User() {
	}


	public void addOrder(Orders order) {
		orders.add(order);
	}
	
	public void removeOrder(Orders order) {
		orders.remove(order);
	}

	public void addComment(Comment cm) {
		comments.add(cm);;
	}
	
	public void removeComment(Comment cm) {
		comments.remove(cm);
	}

	public void addProduct(Product pr) {
		productsAdded.add(pr);
	}
	
	public void removeProduct(Product pr) {
		productsAdded.remove(pr);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Set<Product> getProductsAdded() {
		return productsAdded;
	}

	public void setProductsAdded(Set<Product> productsAdded) {
		this.productsAdded = productsAdded;
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public String getSsnVat() {
		return ssnVat;
	}

	public void setSsnVat(String ssnVat) {
		this.ssnVat = ssnVat;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set<Orders> getOrders() {
		return orders;
	}

	public void setOrders(Set<Orders> orders) {
		this.orders = orders;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}


	public boolean equals(User user) {
        if (!username.equals(user.username)) return false;
        return email.equals(user.email);
    }

	public String getConfirmPasword() {
		return confirmPasword;
	}


	public void setConfirmPasword(String confirmPasword) {
		this.confirmPasword = confirmPasword;
	}
	
    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }

	@Override
	public String toString() {
		return String.format(
				"| %12.12s | %20.20s | %20.20s | %15.15s | %15.15s | %6.6s | %6.6s | %10.10s | %20.20s | %10.10s | %15.15s | %20.20s | Orders: %s | Comments: %s | %30.30s | %30.30s |",
				id, name, email, username, password, gender, role, status, metadata, type, ssnVat, address, orders,
				comments, created, modified);
	}

}
