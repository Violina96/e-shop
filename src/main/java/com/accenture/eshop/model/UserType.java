package com.accenture.eshop.model;

public enum UserType {
	PERSON, CORPORATE;
}
