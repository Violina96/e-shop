package com.accenture.eshop.service;

import java.util.Collection;

import com.accenture.eshop.dto.CommentDto;
import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.Comment;

public interface CommentService {

	Collection<Comment> getComments();
	Collection<Comment> getComments(long id);
	Comment getCommentById(long id) throws NonExistingEntityException;
	Comment deleteCommentById(long id)  throws NonExistingEntityException;
	Comment addComment(CommentDto comment) throws NonExistingEntityException;
	Comment updateComment(Comment comment) throws NonExistingEntityException;
}
