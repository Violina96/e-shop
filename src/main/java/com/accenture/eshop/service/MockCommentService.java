package com.accenture.eshop.service;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.eshop.dao.CommentRepository;
import com.accenture.eshop.dto.CommentDto;
import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.Comment;
import com.accenture.eshop.model.Product;
import com.accenture.eshop.model.User;

@Service
public class MockCommentService implements CommentService {

	private CommentRepository repo;

	@Autowired
	private UserService userService;
	@Autowired
	private ProductService productService;

	@Autowired
	public MockCommentService(CommentRepository repository) {
		this.repo = repository;
	}

	@Override
	public Comment addComment(CommentDto comment) throws NonExistingEntityException {
		User u = userService.getUserById(comment.getAuthorId());
		Product p = productService.getProductById(comment.getProductId());
		Comment cm = new Comment(); 
		cm.setAuthor(u);
		cm.setProduct(p);
		cm.setText(comment.getText());
		cm.setApprovementStatus(comment.getApprovementStatus());
		cm.setUrl(comment.getUrl());
		return repo.save(cm);
	}

	@Override
	public Collection<Comment> getComments() {
		return repo.findAll();
	}

	@Override
	public Comment getCommentById(long id) throws NonExistingEntityException {
		return repo.findById(id)
				.orElseThrow(() -> new NonExistingEntityException("Comment with ID=" + id + " does not exist."));
	}

	@Override
	public Comment deleteCommentById(long id) throws NonExistingEntityException {
		Optional<Comment> old = repo.findById(id);
		old.ifPresent(comment -> repo.deleteById(comment.getId()));
		return old.orElseThrow(() -> new NonExistingEntityException("Comment with ID=" + id + " does not exist."));
	}

	@Override
	public Comment updateComment(Comment comment) throws NonExistingEntityException {
		getCommentById(comment.getId());
		return repo.save(comment);
	}

	@Override
	public Collection<Comment> getComments(long id) {
		Collection<Comment> col = new LinkedHashSet<Comment>();
		for (Comment c : repo.findAll()) {
			if (c.getProduct().getId() == id) {
				col.add(c);
			}
		}
		return col;
	}

}
