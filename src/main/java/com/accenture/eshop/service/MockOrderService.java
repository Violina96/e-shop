package com.accenture.eshop.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.eshop.dao.OrderRepository;
import com.accenture.eshop.dto.OrderDto;
import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.OrderStatus;
import com.accenture.eshop.model.Role;
import com.accenture.eshop.model.User;
import com.accenture.eshop.model.Orders;
import com.accenture.eshop.model.Position;

@Service
public class MockOrderService implements OrderService {
	
	private OrderRepository repo;
	
	@Autowired
	private UserService userService;

	@Autowired
	public MockOrderService(OrderRepository repository) {
		this.repo = repository;
	}
	
	@Override
	public Orders addOrder(OrderDto order) throws NonExistingEntityException {
		User u = userService.getUserById(order.getUserId());
		Orders orders = new Orders();
		orders.setUser(u);
		orders.setStatus(order.getStatus());
		orders.setDescription(order.getDescription());
		orders.setPositions(order.getPositions());
		order.setClosingDate(order.getClosingDate());
		return repo.save(orders);
	}

	@Override
	public Collection<Orders> getOrders() {
		return repo.findAll();
	}

	@Override
	public Orders getOrderById(long id) throws NonExistingEntityException {
		return repo.findById(id)
				.orElseThrow(() -> new NonExistingEntityException("Order with ID=" + id + " does not exist."));
	}

	@Override
	public Orders deleteOrderById(long id) throws NonExistingEntityException {
		Optional<Orders> old = repo.findById(id);
		old.ifPresent(order -> repo.deleteById(order.getId()));
		return old
			.orElseThrow(() -> new NonExistingEntityException("Order with ID=" + id + " does not exist."));
	}

	@Override
	public Orders updateOrder(Orders order) throws NonExistingEntityException {
		getOrderById(order.getId());
		return repo.save(order);
	}

	public void approveOrder(User u, long id, OrderStatus status) {
		if(u.getRole() == Role.ADMIN) {
		repo.findById(id).get().setStatus(status);
		}
	}

	@Override
	public Orders addPosition(Position pos, long id) {
		Orders o = repo.findById(id).get();
		o.addPosition(pos);
		return repo.save(o);
	}

	@Override
	public Orders deletePosition(Position pos, long id) {
		Orders o = repo.findById(id).get();
		o.deletePosition(pos);
		return repo.save(o);
	}

}
