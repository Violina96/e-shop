package com.accenture.eshop.service;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.eshop.dao.ProductRepository;
import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.Comment;
import com.accenture.eshop.model.Orders;
import com.accenture.eshop.model.Position;
import com.accenture.eshop.model.Product;

@Service
public class MockProductService implements ProductService {
	
	private ProductRepository repo;
	
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private UserService userService;
	

	@Autowired
	public MockProductService(ProductRepository repository) {
		this.repo = repository;
	}
	
	@Override
	public Product addProduct(Product product) {
		return repo.save(product);
	}

	@Override
	public Collection<Product> getProducts() {
		return repo.findAll();
	}

	@Override
	public Product getProductById(long id) throws NonExistingEntityException {
		return repo.findById(id)
				.orElseThrow(() -> new NonExistingEntityException("Product with ID=" + id + " does not exist."));
	}

	@Override
	public Product deleteProductById(long id) throws NonExistingEntityException {
		Optional<Product> old = repo.findById(id);
		for(Orders o: orderService.getOrders()) {
			for(Position p : o.getPositions()) {
				if(p.getProduct().getId() == id) {
					throw new IllegalArgumentException("This product cannot be deleted before the order it belongs to is finished!");
				}
			}
		}
		old.ifPresent(product -> repo.deleteById(product.getId()));
		return old
			.orElseThrow(() -> new NonExistingEntityException("Product with ID=" + id + " does not exist."));
	}

	@Override
	public Product updateProduct(Product product) throws NonExistingEntityException {
		getProductById(product.getId());
		return repo.save(product);
	}

	@Override
	public Product addComment(Comment comment, long id) {
		Product p = repo.findById(id).get();
		p.addComment(comment);
		return repo.save(p);
	}
	
	@Override
	public Product deleteComment(Comment comment, long id) {
		Product p = repo.findById(id).get();
		p.removeComment(comment);
		return repo.save(p);
	}
	

	@Override
	public Collection<Product> findByName(String name) {
		Collection<Product> pr = new LinkedHashSet<>();
		for (Product p : repo.findAll()) {
			if (p.getName().equals(name)) {
				pr.add(p);
			}
		}
		return pr;
	}
	
	@Override
	public void deleteAllComments(long id) throws NonExistingEntityException {
		Product p = repo.findById(id).get();
		for (Comment c : p.getComments()) {
			userService.deleteComment(c, c.getAuthor().getId());
			commentService.deleteCommentById(c.getId());
		}
	}
		
	
}