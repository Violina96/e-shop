package com.accenture.eshop.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.accenture.eshop.dao.UserRepository;
import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.Comment;
import com.accenture.eshop.model.Orders;
import com.accenture.eshop.model.Product;
import com.accenture.eshop.model.Role;
import com.accenture.eshop.model.Status;
import com.accenture.eshop.model.User;

@Service
public class MockUserService implements UserService {

	private UserRepository repo;

	@Autowired
	public MockUserService(UserRepository repository) {
		this.repo = repository;
	}

	@Override
	public User addUser(User user) {

		String pass = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
		user.setPassword(pass);
		return repo.save(user);
	}

	@Override
	public Collection<User> getUsers() {
		return repo.findAll();
	}

	@Override
	public User getUserById(long id) throws NonExistingEntityException {
		return repo.findById(id)
				.orElseThrow(() -> new NonExistingEntityException("User with ID=" + id + " does not exist."));
	}

	@Override
	public User deleteUserById(long id) throws NonExistingEntityException {
		Optional<User> old = repo.findById(id);
		old.ifPresent(user -> repo.deleteById(user.getId()));
		return old.orElseThrow(() -> new NonExistingEntityException("User with ID=" + id + " does not exist."));
	}

	@Override
	public User updateUser(User user) throws NonExistingEntityException {
		getUserById(user.getId());
		for (User u : repo.findAll()) {
			if (u.getId() == user.getId()) {
				if (!(user.getUsername().equals(u.getUsername()))) {
					throw new IllegalArgumentException("Username cannot be changed!");
				}
				if (!(user.getEmail().equals(u.getEmail()))) {
					throw new IllegalArgumentException("Email cannot be changed!");
				}
			}
		}
		String pass = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
		user.setPassword(pass);
		return repo.save(user);
	}

	@Override
	public User findByUsername(String username) throws NonExistingEntityException {
		for (User u : repo.findAll()) {
			if (u.getUsername().equals(username)) {
				return u;
			}
		}
		throw new NonExistingEntityException("User with username=" + username + " does not exist.");
	}

	@Override
	public User addComment(Comment comment, long id) {
		User u = repo.findById(id).get();
		u.addComment(comment);
		return repo.save(u);
	}

	@Override
	public User deleteComment(Comment comment, long id) throws NonExistingEntityException {
		User u = repo.findById(id).get();
		u.removeComment(comment);
		return repo.save(u);
	}

	@Override
	public User addProduct(Product pr, long id) {
		User u = repo.findById(id).get();
		u.addProduct(pr);
		return repo.save(u);
	}

	@Override
	public User deleteProduct(Product pr, long id) {
		User u = repo.findById(id).get();
		u.removeProduct(pr);
		return repo.save(u);
	}

	@Override
	public User register(User user) {
		for (User u : repo.findAll()) {
			if (u.getUsername().equals(user.getUsername())) {
				throw new IllegalArgumentException("User with this username already exists!");
			}
			if (u.getEmail().equals(user.getEmail())) {
				throw new IllegalArgumentException("User with this email already exists!");
			}
		}
		if (!user.getPassword().equals(user.getConfirmPasword())) {
			throw new IllegalArgumentException("Passwords do not match!");
		}

		if (repo.findAll().isEmpty()) {
			user.setRole(Role.ADMIN);
		} else {
			user.setRole(Role.CUSTOMER);
		}
		user.setStatus(Status.ACTIVE);
		return user;
	}

	@Override
	public User addOrder(Orders order, long id) {
		User u = repo.findById(id).get();
		u.addOrder(order);
		return repo.save(u);
	}

}