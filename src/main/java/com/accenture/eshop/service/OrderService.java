package com.accenture.eshop.service;

import java.util.Collection;

import com.accenture.eshop.dto.OrderDto;
import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.Orders;
import com.accenture.eshop.model.Position;

public interface OrderService {

	Collection<Orders> getOrders();
	Orders getOrderById(long id) throws NonExistingEntityException;
	Orders deleteOrderById(long id)  throws NonExistingEntityException;
	Orders addOrder(OrderDto order) throws NonExistingEntityException;
	Orders updateOrder(Orders order) throws NonExistingEntityException;
	Orders addPosition(Position pos, long id);
	Orders deletePosition(Position pos, long id);
}
