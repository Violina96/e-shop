package com.accenture.eshop.service;

import java.util.Collection;

import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.Comment;
import com.accenture.eshop.model.Product;

public interface ProductService {

	Collection<Product> getProducts();
	Product getProductById(long id) throws NonExistingEntityException;
	Product deleteProductById(long id)  throws NonExistingEntityException;
	Product addProduct(Product product);
	Product updateProduct(Product product) throws NonExistingEntityException;
	Product addComment(Comment comment, long id);
	Product deleteComment(Comment comment, long id);
	Collection<Product> findByName(String name);
	void deleteAllComments(long id) throws NonExistingEntityException;
}
