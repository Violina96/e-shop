package com.accenture.eshop.service;

import java.util.Collection;

import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.Comment;
import com.accenture.eshop.model.Orders;
import com.accenture.eshop.model.Product;
import com.accenture.eshop.model.User;

public interface UserService {

	Collection<User> getUsers();
	User getUserById(long id) throws NonExistingEntityException;
	User deleteUserById(long id)  throws NonExistingEntityException;
	User addUser(User user);
	User updateUser(User user) throws NonExistingEntityException;
	User findByUsername(String username) throws NonExistingEntityException;
	User addComment(Comment comment, long id);
    User deleteComment(Comment comment, long id) throws NonExistingEntityException;
	User addProduct(Product pr, long id);
	User deleteProduct(Product pr, long id);
	User register(User user);
	User addOrder(Orders order, long id);
}
