package com.accenture.eshop.web;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;


import com.accenture.eshop.exceptions.EntityExistsException;
import com.accenture.eshop.exceptions.NonExistingEntityException;


@ControllerAdvice
@Slf4j
public class AllControllerAdvice {
    @ExceptionHandler({ IllegalArgumentException.class, NonExistingEntityException.class, EntityExistsException.class})
    @Order(1)
    public ModelAndView handle(Exception ex) {
        log.error("Article Controller Error:",ex);
        ModelAndView modelAndView = new ModelAndView("errors");
        modelAndView.getModel().put("message", ex.getMessage());
        return modelAndView;
    }

}
