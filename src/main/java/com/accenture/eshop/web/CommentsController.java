package com.accenture.eshop.web;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.accenture.eshop.dto.CommentDto;
import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.AppStatus;
import com.accenture.eshop.model.Comment;
import com.accenture.eshop.model.Role;
import com.accenture.eshop.service.CommentService;
import com.accenture.eshop.service.ProductService;
import com.accenture.eshop.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class CommentsController {

	@Autowired
	private CommentService service;
	@Autowired
	private ProductService prservice;
	@Autowired
	private UserService uservice;

	@GetMapping("/show_comments")
	public String getComments(Model model, @RequestParam(value = "id") long id, HttpSession session)
			throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		log.debug("GET Comments of this product: " + service.getComments(id));
		model.addAttribute("comments", service.getComments(id));
		if(uservice.getUserById((long)session.getAttribute("userId")).getRole() == Role.ADMIN) {
			return "comment_details_admin";
		}	
		return "comment_details";
	}

	@GetMapping("/add_comment")
	public String addComment(Model model, @RequestParam(value = "id") long id,
			@ModelAttribute("newComment") CommentDto comment, HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		if(uservice.getUserById((long)session.getAttribute("userId")).getRole() == Role.ADMIN) {
			return "new_comment_admin";
		}
		return "new_comment";
	}

	@PostMapping("/add_comment")
	public String addComment(Model model, @RequestParam(value = "id") long id,
			@Valid @ModelAttribute("newComment") CommentDto comment, BindingResult bindingResult, HttpSession session)
			throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		if (bindingResult.hasErrors()) {
			return "new_comment";
		}
		log.info("ADD comment: " + comment);
		comment.setProductId(id);
		comment.setAuthorId((long) session.getAttribute("userId"));
		comment.setApprovementStatus(AppStatus.SUGGESTED);
		Comment cm = service.addComment(comment);
		prservice.addComment(cm, id);
		uservice.addComment(cm, (long) session.getAttribute("userId"));
		model.addAttribute("comments", service.getComments(prservice.getProductById(id).getId()));
		if(uservice.getUserById((long)session.getAttribute("userId")).getRole() == Role.ADMIN) {
			return "comment_details_admin";
		}		
		return "comment_details";
	}

	@GetMapping("/update_comment")
	public String updateComment(Model model, @RequestParam(value = "id") long id,
			@ModelAttribute("comment") Comment comment, HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		if (service.getCommentById(id).getAuthor().getId() == (long) session.getAttribute("userId")
				|| session.getAttribute("role").equals(Role.ADMIN)) {
			model.addAttribute("comment", service.getCommentById(id));
			return "edit_comment";
		}
		throw new IllegalArgumentException("Cannot change comment content!");
	}

	@PutMapping("/update_comment")
	public String updateComment(Model model, @RequestParam(value = "id") long id,
			@Valid @ModelAttribute("comment") Comment comment, BindingResult bindingResult, HttpSession session)
			throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		if (bindingResult.hasErrors()) {
			return "edit_comment";
		}
		log.info("UPDATE comment: " + comment);
		service.updateComment(comment);
		if (comment.getApprovementStatus() == AppStatus.DISAPPROVED) {
			prservice.deleteComment(comment, service.getCommentById(id).getProduct().getId());
			service.deleteCommentById(id);
		}
		model.addAttribute("comments", service.getComments(service.getCommentById(id).getProduct().getId()));
		if(uservice.getUserById((long)session.getAttribute("userId")).getRole() == Role.ADMIN) {
			return "comment_details_admin";
		}	
		return "comment_details";
	}

	@DeleteMapping("/delete_comment")
	public String deleteComment(Model model, @RequestParam(value = "id") long id, HttpSession session)
			throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		log.info("DELETE comment: " + id);
		if (service.getCommentById(id).getAuthor().getId() == (long) session.getAttribute("userId")
				|| session.getAttribute("role").equals(Role.ADMIN)) {
			prservice.deleteComment(service.getCommentById(id), service.getCommentById(id).getProduct().getId());
			uservice.deleteComment(service.getCommentById(id), service.getCommentById(id).getAuthor().getId());
			service.deleteCommentById(id);
			model.addAttribute("products", prservice.getProducts());
			if(uservice.getUserById((long)session.getAttribute("userId")).getRole() == Role.ADMIN) {
				return "product_details";
			}	
			return "products";
		}
		throw new IllegalArgumentException("Cannot delete comment content!");
	}

}