package com.accenture.eshop.web;

import java.time.LocalDateTime;
import java.util.Collection;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.accenture.eshop.dto.OrderDto;
import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.OrderStatus;
import com.accenture.eshop.model.Orders;
import com.accenture.eshop.model.Position;
import com.accenture.eshop.model.Role;
import com.accenture.eshop.service.OrderService;
import com.accenture.eshop.service.ProductService;
import com.accenture.eshop.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class OrdersController {

	@Autowired
	private OrderService service;

	@Autowired
	private ProductService prservice;

	@Autowired
	private UserService uservice;

	@GetMapping("/make_order")
	public String makeOrder(Model model, @ModelAttribute("newOrder") OrderDto order, HttpSession session)
			throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		return "new_order";
	}

	@PostMapping("/make_order")
	public String makeOrder(Model model, @Valid @ModelAttribute("newOrder") OrderDto order, BindingResult bindingResult,
			HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		if (bindingResult.hasErrors()) {
			return "new_order";
		}
		log.info("ADD Order: " + order);
		order.setUserId((long) session.getAttribute("userId"));
		order.setStatus(OrderStatus.ACTIVE);
		Orders o = service.addOrder(order);
		uservice.addOrder(o, (long) session.getAttribute("userId"));
		model.addAttribute("products", prservice.getProducts());
		return "products";
	}

	@GetMapping("/orders")
	public String getOrders(Model model, HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		log.debug("GET Orders: " + service.getOrders());
		Collection<Orders> o = service.getOrders();
		model.addAttribute("orders", o);
		if (uservice.getUserById((long) session.getAttribute("userId")).getRole() == Role.ADMIN) {
			return "admin_orders";
		}
		return "orders";
	}

	@GetMapping("/view_order")
	public String getOrder(Model model, @RequestParam(value = "id") long id, @ModelAttribute("newOrder") Orders order,
			HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		session.setAttribute("orderId", id);
		model.addAttribute("newOrder", service.getOrderById(order.getId()));
		return "view_order";
	}
	
	@GetMapping("/add")
	public String addToOrder(Model model,
			HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		model.addAttribute("products", prservice.getProducts());
		return "products";
	}

	@GetMapping("/add_to_order")
	public String getPosition(Model model, @RequestParam(value = "id") long id,
			@ModelAttribute("newPosition") Position position, HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		if (service.getOrderById((long) session.getAttribute("orderId")).getStatus() != OrderStatus.ACTIVE) {
			throw new IllegalArgumentException("This order has already been finished");
		}
		session.setAttribute("productId", id);
		if(prservice.getProductById(id).getPromotionPercentage() != null) {
		position.setPrice(prservice.getProductById(id).getPrice()-((prservice.getProductById(id).getPrice()*prservice.getProductById(id).getPromotionPercentage())/100));
		} else position.setPrice(prservice.getProductById(id).getPrice());
		return "new_position";
	}

	@PostMapping("/add_to_position")
	public String addPosition(Model model, @Valid @ModelAttribute("newPosition") Position position,
			BindingResult bindingResult, @RequestParam(value = "id") long id, HttpSession session)
			throws NonExistingEntityException {
		if (bindingResult.hasErrors()) {
			return "new_position";
		}
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		log.info("ADD Position to order: " + position);
		position.setProduct(prservice.getProductById(id));
		position.setOrder(service.getOrderById((long) session.getAttribute("orderId")));
		position.setPrice(position.getPositionPrice());
		service.addPosition(position, (long) session.getAttribute("orderId"));
		model.addAttribute("newOrder", service.getOrderById((long) session.getAttribute("orderId")));
		return "view_order";
	}

	@DeleteMapping("/delete_position")
	public String deletePosition(Model model, @RequestParam(value = "id") long id, HttpSession session)
			throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		log.info("DELETE position: " + id);
		for (Position pos : service.getOrderById((long) session.getAttribute("orderId")).getPositions()) {
			if (pos.getId() == id) {
				service.deletePosition(pos, (long) session.getAttribute("orderId"));
				model.addAttribute("newOrder", service.getOrderById((long) session.getAttribute("orderId")));
				return "view_order";
			}
		}
		throw new IllegalArgumentException("Something went wrong! Please try again!");
	}

	@GetMapping("/edit_order")
	public String editOrder(Model model, @RequestParam(value = "id") long id, @ModelAttribute("order") Orders order,
			HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		model.addAttribute("order", service.getOrderById(id));
		return "edit_orders";
	}

	@PutMapping("/edit_order")
	public String editOrder(Model model, @RequestParam(value = "id") long id,
			@Valid @ModelAttribute("order") Orders order, BindingResult bindingResult, HttpSession session)
			throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		if (bindingResult.hasErrors()) {
			return "edit_orders";
		}
		log.info("UPDATE Order: " + order);
		order.setClosingDate(LocalDateTime.now());
		service.updateOrder(order);
		model.addAttribute("orders", service.getOrders());
		return "admin_orders";
	}

}
