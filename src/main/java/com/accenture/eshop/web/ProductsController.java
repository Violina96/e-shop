package com.accenture.eshop.web;

import java.util.Collection;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.Product;
import com.accenture.eshop.service.ProductService;
import com.accenture.eshop.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class ProductsController {

	@Autowired
	private ProductService service;

	@Autowired
	private UserService uservice;


	@GetMapping("/show_products")
	public String showProducts(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		Collection<Product> p = service.getProducts();
		model.addAttribute("products", p);
		log.debug("GET Products: " + service.getProducts());
		return "search_add_product";
	}

	@GetMapping("/search_products")
	public String searchProducts(Model model,
			@RequestParam(value = "name", required = false, defaultValue = "0") String name, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		Collection<Product> filteredProducts;

		if (name.equals("0")) {
			filteredProducts = service.getProducts();
		} else {
			filteredProducts = service.findByName(name);
		}
		model.addAttribute("products", filteredProducts);
		return "product_details";
	}

	@GetMapping("/add_product")
	public String addProduct(Model model, @ModelAttribute("newProduct") Product newProduct, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		return "new_product";
	}

	@PostMapping("/add_product")
	public String addProduct(Model model, @Valid @ModelAttribute("newProduct") Product newProduct,
			BindingResult bindingResult, HttpSession session) {
		if (bindingResult.hasErrors()) {
			return "new_product";
		}
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		log.info("ADD Product: " + newProduct);
		service.addProduct(newProduct);
		uservice.addProduct(newProduct, (long) session.getAttribute("userId"));
		model.addAttribute("products", service.getProducts());
		return "search_add_product";
	}

	@DeleteMapping("/delete_product")
	public String deleteProduct(Model model, @RequestParam(value = "id") long id, HttpSession session)
			throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		log.info("Delete Product: " + id);

		service.deleteAllComments(id);
		uservice.deleteProduct(service.getProductById(id), (long) session.getAttribute("userId"));
		service.deleteProductById(id);
		model.addAttribute("products", service.getProducts());
		return "adminPage";
	}

	@GetMapping("/update_product")
	public String updateProduct(Model model, @RequestParam(value = "id") long id,
			@ModelAttribute("product") Product product, HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		model.addAttribute("product", service.getProductById(id));
		return "edit_product";
	}

	@PutMapping("/update_product")
	public String updateProduct(Model model, @Valid @ModelAttribute("product") Product product,
			BindingResult bindingResult, HttpSession session) throws NonExistingEntityException {
		if (bindingResult.hasErrors()) {
			return "edit_product";
		}
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		log.info("UPDATE Product: " + product);
		service.updateProduct(product);
		model.addAttribute("products", service.getProducts());
		return "search_add_product";
	}

	@GetMapping("/products")
	public String getProducts(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		Collection<Product> p = service.getProducts();
		model.addAttribute("products", p);
		log.debug("GET Products: " + service.getProducts());
		return "products";
	}

}
