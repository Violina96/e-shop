package com.accenture.eshop.web;

import java.util.Collection;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.accenture.eshop.exceptions.NonExistingEntityException;
import com.accenture.eshop.model.Gender;
import com.accenture.eshop.model.Role;
import com.accenture.eshop.model.Status;
import com.accenture.eshop.model.User;
import com.accenture.eshop.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class UsersController {

	@Autowired
	private UserService service;

	@GetMapping("/welcome")
	public String home(Model model) {
		return "welcome";
	}
	
	@GetMapping("/reg")
	public String registerPerson(Model model, @ModelAttribute("newUser") User newUse) {
		return "registration";
	}

	@PostMapping("/reg")
	public String registerPerson(Model model, @Valid @ModelAttribute("newUser") User newUser,
			BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return "registration";
		}
		log.info("Register User: " + newUser);
		User u = service.register(newUser);
		if(u.getPicture()=="") {
			if(u.getGender().equals(Gender.MALE)) {
				u.setPicture("http://www.tsts.telangana.gov.in/assets/img/townpress/noimg.png");
			} else u.setPicture("https://www.abensons.co.uk/wp-content/uploads/2015/06/no-profile-female-240x300.gif");
		}
		model.addAttribute("newUser", service.addUser(u));
		return "login";
	}

	@GetMapping("/login")
	public String logUser(Model model) {
		return "login";
	}

	@PostMapping("/login")
	public String logUser(Model model, @ModelAttribute("user") User user, HttpSession session) throws Exception {
		User u = service.findByUsername(user.getUsername());

		if (BCrypt.checkpw(user.getPassword(), u.getPassword())) {

			session.setAttribute("userId", u.getId());
			session.setAttribute("username", u.getUsername());
			session.setAttribute("role", u.getRole());

			if (u.getRole() == Role.ADMIN) {
				return "adminPage";
			} else {
				return "userPage";
			}
		}
		return "loginFailed";
	}

	@GetMapping("/logout")
	public String logoutUser(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		session.invalidate();
		return "welcome";
	}

	@GetMapping("/adminPage")
	public String loadAdminPage(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		return "adminPage";
	}

	@GetMapping("/userPage")
	public String loadUserPage(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		return "userPage";
	}

	@GetMapping("/show_users")
	public String getUsers(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		Collection<User> u = service.getUsers();
		model.addAttribute("users", u);
		log.debug("GET Users: " + service.getUsers());
		return "user_details";
	}

	@GetMapping("/update_user")
	public String updateUser(Model model, @RequestParam(value = "id") long id, @ModelAttribute("user") User user,
			HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		model.addAttribute("user", service.getUserById(id));
		return "edit_user";
	}

	@PutMapping("/update_user")
	public String updateUser(Model model, @Valid @ModelAttribute("user") User user, BindingResult bindingResult,
			HttpSession session) throws NonExistingEntityException {
		if (bindingResult.hasErrors()) {
			return "edit_user";
		}
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		log.info("Update User: " + user);
		service.updateUser(user);	
		if (user.getStatus() == Status.DEACTIVATED || user.getStatus() == Status.SUSPENDED) {
			service.deleteUserById(user.getId());
		}
		model.addAttribute("users", service.getUsers());
		return "user_details";
	}

	@GetMapping("/show_user")
	public String showUser(Model model, HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		User u = service.getUserById((long) session.getAttribute("userId"));
		model.addAttribute("user", u);
		return "user_profile";
	}

	@GetMapping("/edit_profile")
	public String editProfile(Model model, HttpSession session) throws NonExistingEntityException {
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		model.addAttribute("user", service.getUserById((long) session.getAttribute("userId")));
		return "edit_profile";
	}

	@PutMapping("/edit_profile")
	public String editProfile(Model model, @Valid @ModelAttribute("user") User user, BindingResult bindingResult,
			HttpSession session) throws NonExistingEntityException {
		if (bindingResult.hasErrors()) {
			return "edit_profile";
		}
		if (session.getAttribute("userId") == null) {
			return "login";
		}
		log.info("Update user profile: " + user);
		User u = service.updateUser(user);
		if (user.getStatus() == Status.DEACTIVATED || user.getStatus() == Status.SUSPENDED) {
			service.deleteUserById(user.getId());
			return "login";
		}
		model.addAttribute("user", u);
		return "user_profile";
	}

}
